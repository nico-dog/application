#!/bin/sh

target="SempervirensApp"
config=$1
platform=$2

if [ $platform != "Linux" ]
then
    if [ $platform != "apple" ]
    then
	echo "unrecognized platform"
	return
    fi
fi

builddir="build-Application-"$config'-'$platform
cd ../$builddir

cmake --build . --config $config --target $target --parallel 8

