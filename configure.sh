#!/bin/sh

installdir="/home/nico/Desktop/SempervirensInstall"
#installdir="/opt/dawnofgiantsstudios.com/Sempervirens"
target="SempervirensApp"
config=$1
platform=$2
generator=''

if [ $platform = "Linux" ]
then
    generator="Unix Makefiles"
elif [ $platform = "apple" ]
then
    generator="XCode"
else
    echo "unrecognized platform"
    return
fi

builddir="build-Application-"$config'-'$platform
rm -rf ../$builddir
mkdir ../$builddir && cd ../$builddir

#cmake -G "$generator" -DCMAKE_BUILD_TYPE="$config" ../Application
cmake -G "$generator" -DCMAKE_PREFIX_PATH:PATH=$installdir -DCMAKE_BUILD_TYPE="$config" ../Application

