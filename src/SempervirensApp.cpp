#define SEMPERVIRENSAPP_CPP
#include <SempervirensApp.hpp>

SempervirensApp::SempervirensApp()
{
  SEMPERVIRENS_MSG("size of SempervirensApp: %d\n", sizeof(SempervirensApp));
}

SempervirensApp::~SempervirensApp()
{
  SEMPERVIRENS_MSG("SempervirensApp dtor\n");
}

Block<sempervirens::app::Application> sempervirens::app::createApplication(Arena_t& arena)  
{
  auto app = SEMPERVIRENS_NEW(SempervirensApp, arena);
  return {app._ptr, app._size};
}
