#ifndef SEMPERVIRENSAPP_HPP
#define SEMPERVIRENSAPP_HPP
#include <Sempervirens.hpp>

class SempervirensApp : public sempervirens::app::Application
{
public:
  SempervirensApp();
  ~SempervirensApp();
};

Block<sempervirens::app::Application> sempervirens::app::createApplication(Arena_t& arena);

#endif
